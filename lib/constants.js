export const HOME_OG_IMAGE_URL =
  'https://cdn.statically.io/og/theme=dark/Simple%20Blog.jpg'

export const BLOG_TITLE = 'Riq Blog'

export const BLOG_SUBTITLE = 'Blog Built With NextJs'

export const BLOG_DESCRIPTION = [BLOG_TITLE, BLOG_SUBTITLE].join(' | ')

export const BLOG_AUTHOR = 'Mikail Thoriq'

export const BLOG_AUTHOR_LINK = 'https://blog.mikailthoriq.eu.org/'
