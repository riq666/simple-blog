---
title: 'Hello World'
excerpt: 'Apa itu “hello world” dalam artian bidang programming / coding ?'
coverImage: '/assets/blog/hello-world/cover.jpg'
date: '2021-06-17'
author:
  name: Mikail Thoriq
ogImage:
   url: '/assets/blog/hello-world/cover.jpg'
---

# Apa itu hello world ?

Apa itu “hello world” dalam artian bidang programming / coding ? , Sebuah program yang paling sederhana di dunia. Fungsi utama dari program ini adalah menampilkan pesan ‘hello world’ ke layar.

![contoh kode c](https://partner-mi.riqq-cdn.cf/0:/C-lang-ss_mikailthoriq.png)

*gambar diatas adalah contoh program sederhana “hello world” yang dibuat menggunakan C Lang.*

biasanya program sederhana “hello world” dibuat oleh orang yang ingin mempelajari bahasa pemograman pertama kali.
