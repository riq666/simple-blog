---
title: 'Free Hosting With Cpanel'
excerpt: 'Best hosting with cpanel and unlimited bandwith'
coverImage: '/assets/blog/best-free-hosting-with-cpanel/cover.jpg'
date: '2021-11-24'
author:
  name: Mikail Thoriq
ogImage:
   url: '/assets/blog/best-free-hosting-with-cpanel/cover.jpg'
---

# List Provider Hosting Gratis 

*berdasarkan pengalaman saya sebagai penulis

> https://infinityfree.net (**Rekomended**)

Pro :
  - Unlimited
  - Uptime 99,98%
  - Cpanel
  - Gratis
  - Unlimited Subdomain
  - Unlimited Website/Domain
  - Bisa memakai Cloudflare atau sejenis
  - Bisa memakai domain gratis ex. .cf / .tk
  - Ada script installer ex. wordpress,mediawiki dll
Cons : 
  - Unlimited tapi upload limit di 8mb/item
  - Cpanel fitur tidak selengkap yang di premium
  - Dapet parameter domain aneh *bisa di akalin dibawah 
  - SSL harus bikin sendiri, tidak bisa auto
  - Peraturan yang lumayan ketat
  - Script installer tidak selengkap aslinya (yg ada di premium) 

> https://www.freehosting.com

Pro : 
  - Cpanel full feature
  - Gratis
  - limit upload 1gb/item
  - Dapat disk 5gb
  - Scipt Installer full feature
  - Dapat akun email
Cons :
  - TIDAK BOLEH MENGGUNAKAN HTTPS/SSL
  - Tidak bisa tambah domain
  - Tidak bisa tambahb subdomain
  - Tidak bisa menggunakan Cloudflare atau sejenis
  - Tidak bisa menggunakan domain gratis ex. .cf / .tk
  - Kalau ingin menggunakan HTTPS/SSL harus bayar 24$

> https://xsl.tel (**Rekomended**)

Pro:
  - Cpanel full feature
  - Gratis
  - limit upload 1gb/item
  - Dapat disk 5gb
  - Scipt Installer full feature
  - Dapat akun email
  - Auto SSL
  - Unlimited Subdomain
  - Unlimited Domain
  - Server Cepat
Cons:
  - Tidak boleh menggunakan Cloudflare dkk
  - tidak bisa menggunakan domain gratis*

>https://euserv.com (Recomended)

Pro:
  - VPS (dedicated)
  - bisa menggunakan domain gratis
  - full root ssh
  - dan masih banyak lagi
  - free 1 ip public

Cons:
  - HANYA IPV6
  - Sering2 buka2 google yaa.....

> https://www.hostpoco.com/free-hosting.php

Pro: 
  - ETA

Cons : 
  - ETA

### untuk mengilangkan parameter infinity free taruh dimana saja di .htaccess
```
  RewriteEngine On
  RewriteCond %{QUERY_STRING} ^(.*)i=[^&]+(.*)$ [NC]
  RewriteRule ^(.*)$ /$1?%1%2 [R=301,L] 
```
*Untuk yang domain gratis tidak di terima, bisa menggunakan domain .my.id murah 10-20rb atau https://idcloudhost.com/domain/*



