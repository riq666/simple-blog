---
title: 'Watch Anime With CLI (Linux)'
excerpt: 'A cli to browse and watch anime. (tested on debian / ubuntu based distros)'
coverImage: '/assets/blog/watch-ani-cli/cover.png'
date: '2021-12-06'
author:
  name: Mikail Thoriq
ogImage:
   url: '/assets/blog/watch-ani-cli/cover.jpg'
---

# ani-cli

A cli to browse and watch anime.

This tool scrapes the site [gogoanime](https://www1.gogoanime.cm).

## Download

```bash
git clone https://github.com/pystardust/ani-cli.git
```

## Install

```bash
cd ani-cli
sudo make
```

## Usage

  ### watch anime
  ``ani-cli <query>``

  ### download anime
  ``ani-cli -d <query>``

  ### resume watching anime
  ``ani-cli -H``

  ### delete anime from history
  ``ani-cli -D``

  ### set video quality
  ``ani-cli -q 720``

By default `ani-cli` would try to get the best video quality available  
You can give specific qualities like `360/480/720/..`

You can also use special names:

* `best`: Select the best quality available
* `worst`: Select the worst quality available

Multiple episodes can be viewed/downloaded by giving the episode range like so

  Choose episode [1-13]: 1 6

This would open/download episodes 1 2 3 4 5 6

## Dependencies

* grep

```bash
sudo apt install grep
```

* curl

```bash
sudo apt install curl
```

* sed

```bash
sudo apt install sed
```

* mpv

```bash
sudo apt install mpv
```

* ffmpeg

```bash
sudo apt install ffmpeg
```

