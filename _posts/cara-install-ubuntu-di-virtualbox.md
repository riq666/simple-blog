---
title: 'Cara install ubuntu di virtualbox'
excerpt: 'install ubuntu di virtualbox'
coverImage: '/assets/blog/install-linux/ubuntu.jpg'
date: '2022-10-06'
author:
  name: Mikail Thoriq
ogImage:
   url: '/assets/blog/install-linux/ubuntu.jpg'
---

# Apa itu VirtualBox?

Oracle VM VirtualBox adalah aplikasi virtualisasi lintas platform yang dikembangkan oleh Oracle Corporation. Software ini memungkinkan pengguna untuk menginstal sistem operasi pada hard disk virtual seperti Windows, macOS, Solaris dan Linux.

Sebagai contoh, Anda dapat menjalankan Windows dan Linux di Mac Anda, menjalankan server Windows di server Linux Anda, atau menjalankan Linux di PC Windows Anda sambil menjalankan aplikasi lain yang sudah ada.

Ruang disk dan memori adalah satu-satunya masalah yang akan Anda hadapi saat menginstal beberapa mesin virtual.

Pertama, buka **VirtualBox**, lalu klik “**New**” untuk membuat mesin virtual.

![vboxhome](https://i.ibb.co/XzTN4XX/virbox-home.png)

Masukkan “**Ubuntu**” sebagai namanya, pilih “**Linux**” sebagai jenisnya, dan pilih **Ubuntu (64-bit)** sebagai versinya.

![vboxcreate](https://i.ibb.co/hdvbhpD/vbox-c.png)

Pilih jumlah memori yang Anda inginkan, tetapi ***jangan melebihi dari 50 persen dari total RAM*** yang ada di PC Anda.

![vboxmem](https://i.ibb.co/7b5BN0x/vbox-r.png)

Centang opsi “**Create a virtual hard disk now**” sehingga, kamu nanti akan dapat menentukan ukuran hard disk virtual OS Ubuntu yang akan diinstal.

![vboxcreatehdd1](https://i.ibb.co/2WJ3qtp/vbox-h.png)

Sekarang, kamu memiliki 3 pilihan, harap memilih “**VHD (Virtual Hard Disk)**“.

![vboxcreatehdd2](https://i.ibb.co/rQhnnqS/vbox-ch.png)

Pilih “**Dynamically allocated**” untuk menghemat ruang disk dan memori.

![vboxcreatehdd3](https://i.ibb.co/ryvtyX7/vboc-hcd.png)

Anda harus menentukan **ukuran penyimpanan OS Ubuntu**. Ukuran yang disarankan adalah **10 GB**, tetapi Anda dapat meningkatkan ukurannya jika diinginkan.

![vboxcreatehdd4](https://i.ibb.co/g4PfwT8/vbox-chvd.png)

Setelah membuat hard disk virtual, Anda akan melihat Ubuntu di dasboard VirtualBox Anda.

![vboxhomeafter](https://i.ibb.co/54Y4gGZ/vbox-dashafter.png)

Untuk mengatur file image disk Ubuntu, buka **Settings** dan ikuti langkah-langkah berikut:

![vboxsettings](https://i.ibb.co/9sTJyJ0/vbox-setting.png)

1. Klik “Storage“
2. Di perangkat penyimpanan, klik “Empty“
3. Di tab Attributes, klik gambar disk dan “Choose Virtual Optical Disk File“
4. Pilih file image disk Ubuntu yang telah anda unduh tadi dan buka.

![vboxsettings2](https://i.ibb.co/9sTJyJ0/vbox-setting.png)

Klik OK jika pilihan Anda sudah benar.

OS Ubuntu Anda siap dipasang di VirtualBox. Ayo mulai dengan mengklik Start!

![vboxdashafter](https://i.ibb.co/NWVbGdY/vbox-start.png)

![vboxstart](https://i.ibb.co/ZXKqVRR/vbox-start2.png)

Klik tombol **Install Ubuntu** (Saya sengaja memilih bahasa inggris karena sudah terbiasa).

![vboxinstall](https://i.ibb.co/dPrbqZd/vbox-install1.png)

Pilih tata letak keyboard (**Keyboard layout**) Anda. Klik **Continue** jika sudah selesai.

![vboxinstall2](https://i.ibb.co/6nJHhgk/vbox-install2.png)

Pada bagian “**Updates and other software**“, centang “**Minimal installation**” dan tekan tombol Continue.

![vboxinstall3](https://i.ibb.co/23PrRLp/vbox-install3.png)

Di “**Installation type**“, centang “**Erase disk and install Ubuntu**“. Klik “**Install Now**“.

![vboxinstall4](https://i.ibb.co/5kb0TtC/vbox-install4.png)

Pilih lokasi Anda saat ini, Setelah itu siapkan profil Anda.

![vboxinstall5](https://i.ibb.co/Fhf4NRv/vbox-install5.png)

Anda akan melihat instalasi Ubuntu. Tunggu beberapa menit hingga proses instalasi selesai.

![vboxinstall6](https://i.ibb.co/fMYbhhY/vbox-install6.png)

Setelah penginstalan, mulai ulang atau **Restart Now**.

![vboxinstall7](https://i.ibb.co/SdbTX6M/vbox-install7.png)

Setelah Logging in, Anda akan melihat **desktop Ubuntu**.

![vboxinstall8](https://i.ibb.co/p3x12Hn/vbox-installdone.png)









